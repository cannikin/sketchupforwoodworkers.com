ignore '.ruby-version', /\.git/, /\.sass-cache/, /\/_.*/, /Gemfile/

layout 'layout.html.slim'

before 'index.html.slim' do
  @title = 'Sketchup for Woodworkers'
end

before 'tutorials/getting-started-part-1.html.slim' do
  @title = 'Getting Started, Part 1 | Sketchup for Woodworkers'
end

before 'tutorials/getting-started-part-2.html.slim' do
  @title = 'Getting Started, Part 2 | Sketchup for Woodworkers'
end

before 'tutorials/dados-rabbets-grooves.html.slim' do
  @title = 'Dados, Rabbets and Grooves | Sketchup for Woodworkers'
end

before 'tutorials/curves-moulding-outliner.html.slim' do
  @title = 'Curves, Moulding and the Outliner | Sketchup for Woodworkers'
end

before 'tutorials/moulding-revisited.html.slim' do
  @title = 'Moulding Revisited | Sketchup for Woodworkers'
end

before 'tutorials/joints.html.slim' do
  @title = 'Joints, Joints, Joints | Sketchup for Woodworkers'
end

before 'tutorials/dimensions-printing.html.slim' do
  @title = 'Dimensions and Printing | Sketchup for Woodworkers'
end
